import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";
import './styles/index.css';

//const element = <h1>Привет, мир</h1>;
ReactDOM.render(<App />, document.getElementById('root'));
