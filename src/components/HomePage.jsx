import React from "react";
import { connect } from "react-redux";

function HomePage(props) {
  return (
    <div className="post">
      <h1 className="homePageTitle" >{props.home.title}</h1>
      <img className="homePageImage" src={props.home.picture} alt={props.home.title}></img>
      <div className="homePageDescription">{props.home.description}</div>
    </div>
  );
}

const mapStateToProps = (state) => ({
    home: state.home
});

export default connect(mapStateToProps)(HomePage);
