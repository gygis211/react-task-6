import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

function Admin(props) {
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [picture, setImage] = useState();
  const [isEdit, setIsEdit] = useState(false);

  useEffect(() => {}, [name, description, picture]);

  const sendNewProduct = () => {
    let product = {
      name: name,
      description: description,
      picture: picture,
    };

    if (isEdit) {
      props.editProduct(product);
    } else {
      props.addProduct(product);
    }

    setName("");
    setDescription("");
    setImage("");
    setIsEdit(false);
  };

  const handleEdit = (object) => {
    setIsEdit(true);
    setName(object.name);
    setDescription(object.description);
    setImage(object.picture);
  };

  const handleDelete = (object) => {
    props.deleteProduct(object.id);
  };

  return (
    <div>
      <div>
        <h1>Создать товар</h1>
      </div>
      <div className="post">
        <span>Название: </span>{" "}
        <input className="inputForm" value={name} onChange={(e) => setName(e.target.value)} />
        <br />
        <span>Изображение: </span>{" "}
        <input className="inputForm" value={picture} onChange={(e) => setImage(e.target.value)} />
        <br />
        <span>Описание: </span>{" "}
        <textarea
          className="inputForm"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <br />
        <button onClick={sendNewProduct}>Отправить</button>
      </div>
      <div>
        {props.news.map((object) => (
          <div className="post" key={object.id}>
            <div>
              <h1 className="news">{object.name}</h1>
              <img className="img" src={object.picture} alt={object.name} />
              <p>{object.description}</p>
            </div>
            <button onClick={() => handleEdit(object)}>Изменить</button>
            <button onClick={() => handleDelete(object)}>Удалить</button>
          </div>
        ))}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  news: state.news,
});

const mapDispatchToProps = (dispatch) => ({
  addProduct: (data) => {
    dispatch({ type: "ADD_NEWS", data });
  },
  editProduct: (data) => {
    dispatch({ type: "EDIT_NEWS", data });
  },
  deleteProduct: (id) => {
    dispatch({ type: "DELETE_NEWS", id });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
